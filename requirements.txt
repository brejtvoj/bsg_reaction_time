# Data collection
unidecode
numpy
scipy
datetime
python-csv
pygame
opencv-python

# Analysis (not needed to run - comment out)
panda
seaborn
matplotlib
scikit-learn
statsmodels
