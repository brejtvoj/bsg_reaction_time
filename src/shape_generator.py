import time

import numpy as np
from scipy.ndimage import gaussian_filter
from scipy.stats import multivariate_normal

import cv2

def rotate_image(image: np.ndarray, angle: float):
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(
        image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result


class Shape:
    def __init__(self) -> None:
        # Geometrical properties
        self.shape: np.ndarray = None
        self.position: tuple = None  # Set after placing in the window
        self.size: tuple = None
        self.volume: int = None
        self.alpha_mask: np.ndarray = None
        self.shape_type: str = None

        # Photometrical propeties
        self.texture: str = None
        self.texture_density: float = None
        self.colors: list = None

        # Timing
        self.show_time: float = None  # Set after placing in the window
        self.reaction_time: float = None  # Set after user clicked on the shape

    def generate_shape(self, N_points: int, diff: float, size: int,
                       is_smooth: bool, smoothness: float = 0, hard_edges: bool = True) -> None:
        if is_smooth:
            self.shape_type = 'smooth'
            self.shape = generate_soft_shape(
                N_points, diff, size, smoothness=smoothness, hard_edges=hard_edges)
        else:
            self.shape_type = 'hard'
            self.shape = generate_sharp_shape(N_points, diff, size)

        # Create image from the mask
        self.size = self.shape.shape
        self.volume = np.sum(self.shape)
        self.alpha_mask = self.shape.copy()
        self.shape = np.repeat(self.shape[:, :, np.newaxis], 3, axis=2)
        self.shape = self.shape.astype(int)

    def add_texture(self, texture: str = None, density: float = 0.05) -> None:
        # No texture
        if not texture:
            return

        size = 3 * self.size
        # Generate various textures
        if texture == 'points':
            radius = min(size[0], size[1]) * density
            xx = np.arange(3 * radius) - 1.5 * radius
            X, Y = np.meshgrid(xx, xx)
            coord = np.concatenate(
                (X[:, :, np.newaxis], Y[:, :, np.newaxis]), axis=2)
            circle = np.sqrt(np.sum(coord**2, axis=2)) <= radius
            y_s, x_s = circle.shape
            texture_ = np.tile(
                circle, (1 + size[0] // y_s, 1 + size[1] // x_s))
            texture_ = 1.0 - texture_.astype(float)

        elif texture == 'lines':
            i = np.argmax(size)
            thickness = int(size[i] * density)
            line = np.zeros((size[i], 2 * thickness))
            line[:, :thickness] = 1.0
            texture_ = np.tile(line, reps=1 + int(size[1-i] // thickness))
            if np.argmax(texture_.shape) != i:
                texture_ = np.rot90(texture_)

        elif texture == 'chessboard':
            thickness = int(max(size[0], size[1]) * density)
            box = np.zeros((thickness, 2 * thickness))
            box[:, :thickness] = 1.0
            texture_x = np.tile(box, 1 + int(size[1] // thickness / 2))
            texture_xx = np.concatenate(
                (texture_x, np.fliplr(texture_x)), axis=0)
            texture_ = np.tile(texture_xx.T, 1 +
                               int(size[0] // (2 * thickness))).T
        else:
            raise NotImplementedError(
                f'{texture} is not supported. Use [points, lines, chessboard, sticks]')

        # Apply texture
        self.texture = texture
        self.texture_density = density

        self.shape = self.shape.astype(float)
        texture_ = np.tile(texture_, (4, 4))
        texture_ = rotate_image(texture_, np.random.uniform(-45, 45))
        texture_ = gaussian_filter(texture_, sigma=0.5)
        alpha = 0.4
        texture_ = (1 - alpha) * texture_ + alpha

        # Center crop
        Ht, Wt = texture_.shape
        Hs, Ws = self.size
        texture_ = texture_[int(Ht / 2 - Hs / 2): int(Ht / 2 + Hs / 2),
                            int(Wt / 2 - Ws / 2): int(Wt / 2 + Ws / 2)]
        texture_ = np.repeat(texture_[:, :, np.newaxis], 3, 2)
        self.shape *= texture_
        self.shape = self.shape.astype(int)

    def add_color(self, colors: dict) -> None:

        # Save colors
        self.colors = list(colors.keys())

        # Create meshgrid
        y = np.arange(self.size[0]) - self.size[0] // 2
        x = np.arange(self.size[1]) - self.size[1] // 2
        X, Y = np.meshgrid(x, y)
        coords = np.zeros((self.size[0], self.size[1], 2))
        coords[:, :, 0] = X
        coords[:, :, 1] = Y

        probs = np.zeros((self.size[0], self.size[1], len(colors)))

        # Generate len(colors) multivariate normal distributions
        for i, c in enumerate(colors.values()):
            x_c = np.random.randint(0, self.size[0]) - self.size[0] // 2
            y_c = np.random.randint(0, self.size[1]) - self.size[1] // 2
            mean = np.array([y_c, x_c])

            C = np.eye(2) + np.random.uniform(0, 0.2, (2, 2))
            C *= np.random.uniform(0.85, 1.15, (2, 2))
            C *= self.size
            C *= 10

            multivar = multivariate_normal(mean, C, allow_singular=False)
            probs[:, :, i] = multivar.pdf(coords)

        # P(color=c|(y, x))
        probs /= np.repeat(probs.sum(axis=2)[:, :, np.newaxis], len(colors), 2)
        probs = np.repeat(probs[:, :, np.newaxis, :], 3, 2)

        # Colorize
        color_final = np.zeros((self.size[0], self.size[1], 3))
        blank = np.ones_like(color_final)
        for i, c in enumerate(colors.values()):
            color_single = c * blank
            color_final += probs[:, :, :, i] * color_single
        color_final = np.round(color_final, 0).astype(int)
        # Place onto mask

        color_final[self.shape == 0] = 0
        self.shape = color_final

    def show(self) -> None:
        print('GEOMETRY\n-------------')
        print(f'Size: {self.size}')
        print(f'Position: {self.position}')
        print('PHOTOMETRY\n-------------')
        print(f'Texture: {self.texture}')
        print(f'Colors: {self.colors}')
        print('TIMING\n-------------')
        print(f'Show time: {self.show_time}')
        print(f'Reaction time: {self.reaction_time}')
        print('-------------')

        import matplotlib.pyplot as plt

        # Force format
        s_plt = self.shape.astype(int)
        s_plt = np.clip(
            255 * s_plt / (np.max(s_plt, axis=(0, 1)) + 1e-10), 0, 255).astype(int)
        s_plt[np.sum(s_plt, axis=2) == 0] = 255
        plt.imshow(s_plt)
        plt.xticks([])
        plt.yticks([])
        plt.box(False)
        plt.show()


def generate_sharp_shape(N_points: int, diff: float, size: int) -> np.ndarray:
    assert N_points >= 3

    # Generate point on a circle
    angle = np.linspace(0, 2 * np.pi - 2 * np.pi / N_points, N_points) + \
        np.random.uniform(-np.pi / 6, np.pi / 6, N_points)
    px = np.cos(angle)
    py = np.sin(angle)
    points = np.vstack((px, py))

    # Randomize points
    ps = points + np.random.uniform(-diff, diff, points.shape)
    ps *= size

    # Move to non-negative values
    ps[0, :] -= np.min(ps[0, :])
    ps[1, :] -= np.min(ps[1, :])

    # Generate canvas
    x_dim = np.ceil(np.max(ps[0, :])).astype(int) + 1
    y_dim = np.ceil(np.max(ps[1, :])).astype(int) + 1
    canvas = np.zeros((y_dim, x_dim), dtype=int)

    # Draw lines connecting points
    for i in range(N_points):
        x1, y1 = ps[:, i]
        x2, y2 = ps[:, (i + 1) % N_points]

        N_diff = 5 * max(np.round(np.abs(x1 - x2), 0),
                         np.round(np.abs(y1 - y2), 0)).astype(int)
        x_diff = np.linspace(x1, x2, N_diff, dtype=int)
        y_diff = np.linspace(y1, y2, N_diff,  dtype=int)

        canvas[y_diff, x_diff] = 1

    canvas

    # Fill canvas - code stolen, not sure where from, idk, idc
    canvas = np.maximum.accumulate(canvas, 1) & np.maximum.accumulate(
        canvas[:, ::-1], 1)[:, ::-1]

    return canvas


def generate_soft_shape(N_points: int, diff: float, size: int, smoothness: float, hard_edges: bool) -> np.ndarray:

    # Generate sharp shape
    canvas = generate_sharp_shape(N_points, diff, size)
    x_dim, y_dim = canvas.shape

    # Place into new canvas - preventing out of bounds
    edge = int(smoothness // 1 + 1)
    canvas_smooth = np.zeros((x_dim + 2 * edge, y_dim + 2 * edge))
    canvas_smooth[edge:-edge, edge:-edge] = canvas

    # Blur
    canvas_smooth = gaussian_filter(canvas_smooth, sigma=smoothness)
    # {0, 1} if hard_edges, else <0, 1>
    if hard_edges:
        canvas_smooth = canvas_smooth > 0.3

    # Crop
    position = np.argwhere(canvas_smooth != 0)
    x0 = np.min(position[:, 0]) - 1
    x1 = np.max(position[:, 0]) + 1
    y0 = np.min(position[:, 1]) - 1
    y1 = np.max(position[:, 1]) + 1
    canvas_smooth = canvas_smooth[x0:x1, y0:y1]

    return canvas_smooth


def get_shape(N_points: int, diff: float, size: int,
              is_smooth: bool, smoothness: float,
              colors: list,
              texture: str, density: float,
              verbose: bool = False) -> Shape:
    """
    Generate Shape object (wrapper around numpy array), from given arguments.
    # TODO Add documentation
    """
    if verbose:
        import time
        t1 = time.time()
    S = Shape()
    S.generate_shape(N_points, diff, size, is_smooth, smoothness)
    S.add_color(colors)
    S.add_texture(texture, density)
    if verbose:
        print(f'Generated shape in {round(time.time() - t1, 3)} s.')
        S.show()
    return S

