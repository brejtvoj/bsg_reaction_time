COLORS = {
    'red': [255, 0, 0],
    'orange': [255, 102, 0],
    'yellow': [255, 255, 0],
    'green': [0, 128, 0],
    'blue': [0, 0, 255],
    'purple': [128, 0, 128],
    'pink': [255, 192, 203],
    'turquoise': [64, 224, 208],
    'brown': [115, 65, 7],
    'white': [255, 255, 255]
}

BG_COLOR = [128, 128, 128]
TXT_COLOR = [190, 190, 190]

TEXTURES = [None, 'points', 'lines', 'chessboard']

sequence_length = 30

window_size = (1920, 1080)

mean_show_time = 2.0

CONSTANT_SEED = 6942069 # Totally random number
