import csv
import os
from datetime import datetime

from constants import *
from shape_generator import Shape

def save_to_csv(reaction_times: list[float], sequence: list[Shape], error_count: list[int], challenge_mode: bool, name: str) -> None:
    header = ['size', 'volume', 'position', 'shape_type', 'texture',
              'colors', 'show_time', 'reaction_time', 'error_count']
    data = []
    for s, rt, e in zip(sequence, reaction_times, error_count):
        data_row = [s.size, s.volume, s.position, s.shape_type,
                    'None' if not s.texture else s.texture, s.colors, s.show_time, rt, e]
        data.append(data_row)

    now = datetime.now()
    recording_time = now.strftime("%d_%m_%H_%M")


    if challenge_mode:
        if not os.path.exists('challenge'): os.mkdir('challenge')
        path = os.path.join('challenge', f'{name}.csv')
    else:
        if not os.path.exists('results'): os.mkdir('results')
        path = os.path.join('results', f"{name}_{recording_time}.csv")
    
    with open(path, 'w', encoding='UTF', newline='') as f:
        writer = csv.writer(f, delimiter=",")
        writer.writerow(header)
        writer.writerows(data)
