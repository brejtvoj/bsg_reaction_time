import random as r

import numpy as np
import numpy.random as npr
from constants import *
from shape_generator import Shape, get_shape


def generate_sequence(sequence_length: int, window_size: tuple, mean_show_time: float,
                      verbose: bool = True) -> list[Shape]:
    # Shape style
    seq_l_orig = sequence_length
    sequence_length += 25

    is_smooth = npr.uniform(0, 1, sequence_length) >= 0.5

    # Number of points in the shape
    point_lower = 3 + is_smooth * 7
    point_upper = 10 + is_smooth * 30
    N_points = [npr.randint(pl, pu)
                for pl, pu in zip(point_lower, point_upper)]

    # Randomness of points
    diff = npr.uniform(0, 1, sequence_length)

    # Size
    size = npr.randint(int(0.075 * min(*window_size)),
                       int(0.15 * min(*window_size)), sequence_length)
    smoothness = size * 0.1

    # Color
    color_name = list(COLORS.keys())
    color_names = [npr.choice(color_name,
                              npr.randint(1, 4), replace=False)
                   for _ in range(sequence_length)]

    color_value = [{ci: COLORS[ci] for ci in c} for c in color_names]

    # Texture - Half of the time no texture
    texture = npr.choice(TEXTURES, sequence_length, replace=True, p=[0.5, 0.5/3, 0.5/3, 0.5/3])
    density = npr.uniform(0.05, 0.08, sequence_length)

    variables = zip(N_points, diff, size, is_smooth,
                    smoothness, color_value, texture, density)

    # Generate the whole sequence
    sequence: list[Shape] = list()
    e_count = 0
    if verbose:
        print('Generating sequence.')
    for i, var in enumerate(variables):
        try:
            sequence.append(get_shape(*var, verbose=verbose))
        except Exception:
            e_count += 1
            i -= 1
        if verbose:
            print(f' {str(len(sequence)).rjust(2)} / {seq_l_orig}', end='\r')
        if len(sequence) == seq_l_orig: break
    if verbose:
        print(f'\nSequence generated. (E = {e_count})')

    # Set time and position
    for s in sequence:
        s.show_time = np.clip(npr.exponential(
            mean_show_time) + 1.0, 0, 5) + np.random.uniform(-0.5, 0.5)
        s.position = (npr.randint(
            0, window_size[0] - s.size[0] - 1), npr.randint(0, window_size[1] - s.size[1] - 1))

    return sequence


if __name__ == "__main__":
    seq = generate_sequence(300, (1920, 1080), 3, verbose=True)