import os
import sys
import time as t

import numpy as np
from unidecode import unidecode

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame
from pygame.locals import *

from constants import *
from data_managment import save_to_csv
from sequence_generator import generate_sequence


def is_clicked(object_pos: tuple, object_size: tuple, mouse_pos: tuple, surf: pygame.Surface, mode: bool) -> bool:
    if mode: return True

    l = mouse_pos[0] >= object_pos[0]
    r = mouse_pos[0] <= object_pos[0] + object_size[0]
    u = mouse_pos[1] >= object_pos[1]
    d = mouse_pos[1] <= object_pos[1] + object_size[1]
    if not (l and r and u and d): return False
    
    x = mouse_pos[0] - object_pos[0]
    y = mouse_pos[1] - object_pos[1]
    
    try:
        val = surf.get_at([x, y])[:3]
        if sum(val) == 0: return False
    except IndexError:
        return False
    
    return True


def reaction_game() -> None:
    # Input parsing
    if len(sys.argv) < 2:
        raise UserWarning('Provide user name and mode.')
    
    name = unidecode(sys.argv[1]).replace(' ', '').replace('_', '')
    challenge_mode = '-C' in sys.argv or '-c' in sys.argv
    fixed_mode = '-F' in sys.argv or '-f' in sys.argv
    homicide = '-E' in sys.argv
    
    if not (challenge_mode or fixed_mode):
        raise UserWarning('Running program without any flags - please select [-F, -C]')

    # Same sequence for everyone in the challenge mode
    if challenge_mode:
        np.random.seed(CONSTANT_SEED)
    else:
        np.random.seed(int(t.time()))

    # Time handling
    t_shown = None
    t_clicked = None
    t_current = None
    t_start = -1

    # Object handling
    global sequence_length
    i = 0
    is_shown = False
    object_position = None
    object_size = None
    sequence = generate_sequence(
        sequence_length, window_size, mean_show_time, verbose=False)
    sequence_length = len(sequence)
    reaction_times = [0 for _ in range(sequence_length)]
    surfaces = [pygame.surfarray.make_surface(s.shape) for s in sequence]

    error_count = [0 for _ in range(sequence_length)]

    # Game state handling
    pygame.init()
    running = True
    screen = pygame.display.set_mode(window_size)
    screen.fill(BG_COLOR)
    pygame.display.set_caption(f'ReactGame - {i + 1} / {sequence_length}')

    # Hide cursor
    if fixed_mode:
        pygame.mouse.set_cursor((8,8),(0,0),(0,0,0,0,0,0,0,0),(0,0,0,0,0,0,0,0))

    # Progress text
    font_intro = pygame.font.Font(os.path.join('assets', 'arial.ttf'), 64)
    if fixed_mode:
        string = 'Press spacebar, once shape appears.'
    elif challenge_mode:
        string = 'Click on shapes, as they appear.'

    text = font_intro.render(string, True, COLORS['white'], BG_COLOR)
    rect = text.get_rect()
    rect.left = window_size[0] // 2 - text.get_size()[0] // 2
    rect.top = window_size[1] // 2 - 64
    text.set_colorkey(BG_COLOR)
    screen.blit(text, rect)
    pygame.display.update()
    t.sleep(3)
    screen.fill(BG_COLOR)

    font = pygame.font.Font(os.path.join('assets', 'arial.ttf'), 32)
    text = font.render(string, True, COLORS['white'], BG_COLOR)
    rect = text.get_rect()
    rect.left = 10
    rect.top = 10

    pygame.event.clear()

    while running:
        if i == sequence_length: break
        if challenge_mode and sum(error_count) >= sequence_length: break

        text = font.render(f'{i}/{len(sequence)}', True, TXT_COLOR, BG_COLOR)
        text.set_colorkey(BG_COLOR)

        screen.blit(text, rect)        

        # No shape shown - reset timer
        if t_start == -1:
            t_start = t.time()

        # Time to show the shape
        t_current = t.time()
        if (t_current - t_start >= sequence[i].show_time):
            if not is_shown:
                is_shown = True
                surf = surfaces[i]
                surf.set_colorkey((0, 0, 0))
                object_position = sequence[i].position
                object_size = surf.get_size()
                screen.blit(surf, object_position)
                t_shown = t.time()

        for event in pygame.event.get():
            try:
                p = event.dict['text'] == ' '
            except KeyError:
                p = False
            if event.type == QUIT:
                running = False
            elif event.type == MOUSEBUTTONDOWN or (fixed_mode and p):
                # If clicked when no shape is shown display danger color (Possible epilepsy warning)
                pos = event.pos if not fixed_mode else (-1, -1)
                if not is_shown or not is_clicked(object_position, object_size, pos, surf, mode=fixed_mode):
                    error_count[i] += 1
                    screen.fill(COLORS['orange'])
                    pygame.display.update()
                    pygame.time.wait(int(1000 / 24))
                    screen.fill(BG_COLOR)
                    if is_shown:
                        screen.blit(surf, object_position)
                    pygame.display.update()
                    continue

                # If shown and clicked, hide shape, save time and wait for the next
                t_clicked = t.time()
                reaction_times[i] = t_clicked - t_shown
                screen.fill(BG_COLOR)
                is_shown = False
                i += 1
                pygame.display.set_caption(
                    f'ReactGame - {i + 1} / {sequence_length}')
                t_start = -1

                # End of sequence reached
                if i == sequence_length:
                    running = False
            
            # hihi - u dead - epilepsy warning
            elif event.type == MOUSEMOTION and homicide:
                c = np.random.randint(30, 255, 3)
                screen.fill((c[0], c[1], c[2]))
                if is_shown:
                    screen.blit(surf, object_position)

        
        pygame.display.update()

    if challenge_mode and i != sequence_length and sum(error_count) >= sequence_length:
        string = f'Too many errors ({sum(error_count)})\nDisqualified'
        font_size = 64
        font = pygame.font.Font(os.path.join('assets', 'arial.ttf'), 128)
        screen.fill(BG_COLOR)
        for j, l in enumerate(string.splitlines()):
            text = font.render(l, True, COLORS['white'], BG_COLOR)
            x = window_size[0] // 2 - text.get_size()[0] // 2
            y = window_size[1] // 2 - 2 * 128
            text.set_colorkey(BG_COLOR)
            screen.blit(text, (x, y + 2 * j * font_size))
        pygame.display.update()
        t.sleep(4)

    if challenge_mode and i == sequence_length and not fixed_mode:
        disqualified = sum(error_count) >= sequence_length
        if disqualified:
            string = f'Too many errors ({sum(error_count)})\nDisqualified'
            is_new_best = False
        else:
            mean_reaction_time = np.array(reaction_times).mean()
            with open(os.path.join('challenge', 'best_time.txt'), 'r') as f:
                best_time = float(f.readline())
                best_name = f.readline()
            is_new_best = best_time > mean_reaction_time
            best_time = min(mean_reaction_time, best_time)
            if is_new_best: best_name = name
            string = f'Sequence completed\nYour mean time: {mean_reaction_time.round(3)}s\n' + f'Best time: {round(best_time, 3)}s ({best_name})\n'
            string += '' if not is_new_best else 'New BEST TIME!'

        font_size = 64
        font = pygame.font.Font(os.path.join('assets', 'arial.ttf'), font_size)
        for j, l in enumerate(string.splitlines()):
            text = font.render(l, True, COLORS['white'], BG_COLOR)
            x = window_size[0] // 2 - text.get_size()[0] // 2
            y = window_size[1] // 2 - 2 * font_size
            text.set_colorkey(BG_COLOR)
            screen.blit(text, (x, y + 2 * j * font_size))

        pygame.display.update()
        if is_new_best:
            with open(os.path.join('challenge', 'best_time.txt'), 'w') as f:
                f.write(f'{str(mean_reaction_time)}\n')
                f.write(f'{name}')
        
        t.sleep(6)

        if disqualified:
            pygame.quit()
            return
        
    elif fixed_mode and i == sequence_length:
        mean_reaction_time = np.array(reaction_times).mean()
        string = f'Sequence completed\nYour mean time: {mean_reaction_time.round(3)}s\n'
        font_size = 128
        font = pygame.font.Font(os.path.join('assets', 'arial.ttf'), font_size)
        for j, l in enumerate(string.splitlines()):
            text = font.render(l, True, COLORS['white'], BG_COLOR)
            x = window_size[0] // 2 - text.get_size()[0] // 2
            y = window_size[1] // 2 - 2 * font_size
            text.set_colorkey(BG_COLOR)
            screen.blit(text, (x, y + 2 * j * font_size))

        pygame.display.update()
        t.sleep(4)


    pygame.quit()


    if i == sequence_length:
        save_to_csv(reaction_times, sequence, error_count, challenge_mode, name)
    else:
        raise UserWarning('Sequence ended before reaching end - no data was saved.')


if __name__ == "__main__":
    reaction_game()
