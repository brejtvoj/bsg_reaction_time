Usage:

Parameters can be adjusted in the constants.py file.

Pipeline:

0. Run `pip install --upgrade pip` and `pip install -r requirements.txt` (can take a minute)
1. Start the main.py (located in 'src')

    FLAGS   

        - "NAME" (required)

        - "-F" (Fixed mode - no precise clicking, just press SPACEBAR or LMB)

        - "-C" (optional challenge mode)

        Example: '.../path_to_main.py' -JohnDoe -F

2. By default, 30 shapes with varying parameters are presented.
3. Reaction time for each is recorded.
4. Reaction time along the shape parameters are saved into a csv file.
